Feature: Navigation to different destination through Hamburger Menu

  @Positive
  Scenario Outline: Navigate to desired location
    Given I am at homepage
    When I click on Hamburger menu
    And I select "<Destination>" destination from the menu list
    Then I should see the result of "<Destination>" destination
    Examples: 
      | Destination |
      | Southeast   |
      | Northeast   |
