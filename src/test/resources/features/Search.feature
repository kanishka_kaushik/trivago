Feature: I want to verify the search functionality of the application

  @Positive
  Scenario: Verify the search functionality with valid keyword
    Given I am at homepage
    When I click on search icon
    And I enter "resorts" keyword in the search textbox
    Then I should see the search results on the search result page
    And I should see the search result count on the top of page

@Negative
  Scenario: Verify the search functionality with invalid keyword
    Given I am at homepage
    When I click on search icon
    And I enter "sdfsdf" keyword in the search textbox
    Then I should see the no results message on the search result page

    