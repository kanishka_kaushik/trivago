package stepdefs;

import org.testng.Assert;

import cucumber.api.java.en.Then;

public class DestinationPageSteps extends Core{
	
	@Then("^I should see the result of \"([^\"]*)\" destination$")
	public void I_see_destination_result(String arg1) throws Throwable {
		Assert.assertTrue(assertIsElementPresent("//*[@id='articleheader'][contains(text(), '" + arg1 + "')]"));
		Assert.assertTrue(assertIsElementPresent("//*[@id='more_about']/h1[contains(text(), '" + arg1 + "')]"));
	}

}
