package stepdefs;

import org.openqa.selenium.By;
import org.testng.Assert;

import cucumber.api.java.en.Then;

public class SearchResultPageSteps extends Core{
	
	@Then("^I should see the search results on the search result page$")
	public void i_should_see_the_search_results_on_the_search_result_page() throws Throwable {
		Assert.assertTrue(isElementPresent("searchResult"));
	}

	@Then("^I should see the search result count on the top of page$")
	public void i_should_see_the_search_result_count_on_the_top_of_page() throws Throwable {
		String[] parts = driver.findElement(By.xpath(object.getProperty("resultCount"))).getText().split(" ", 2);
		Assert.assertNotEquals((Integer.parseInt(parts[0])), 0);
	}

	@Then("^I should see the no results message on the search result page$")
	public void i_should_see_the_no_results_message_on_the_search_result_page() throws Throwable {
		driver.findElement(By.xpath(object.getProperty("noResult"))).getText();
		Assert.assertEquals(driver.findElement(By.xpath(object.getProperty("noResult"))).getText().toLowerCase(), "no results");
	}

}
