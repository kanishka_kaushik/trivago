package stepdefs;


import cucumber.api.java.After;
import cucumber.api.java.Before;

public class ServiceHooks extends Core{

	Core core = new Core();
	
	@Before
	public void initialize() {

		core.initialize();
	}
		

	@After
	public void tearDown(){
		core.closeBrowser();
	}
}
