package stepdefs;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Core {
	
	public static WebDriver driver;
	public static FileInputStream fis;
	public static Properties object = new Properties();
	
	public void initialize(){
		
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\resources\\object.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			object.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20L, TimeUnit.SECONDS);
	}
	
	public static boolean isElementPresent(String xpath){
		
		try{
			
			driver.findElement(By.xpath(object.getProperty(xpath)));
			return true;
			
		}catch(Throwable t){
			
			return false;
			
		}
		
	}
	
	public static boolean assertIsElementPresent(String xpath){
		
		try{
			
			driver.findElement(By.xpath(xpath));
			return true;
			
		}catch(Throwable t){
			
			return false;
			
		}
		
	}
	
	public void closeBrowser(){
		driver.close();
	}

}
