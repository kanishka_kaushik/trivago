package stepdefs;

import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class HomePageSteps extends Core{
	
@Given("^I am at homepage$")
public void i_am_at_homepage() throws Throwable {
	
	driver.get("http://room5.trivago.com/");
}

@When("^I click on search icon$")
public void i_click_on_search_icon() throws Throwable {
	driver.findElement(By.xpath(object.getProperty("searchIcon"))).click();;
}

@When("^I enter \"([^\"]*)\" keyword in the search textbox$")
public void i_enter_keyword_in_the_search_textbox(String arg1) throws Throwable {
	driver.findElement(By.className(object.getProperty("searchTextbox"))).sendKeys(arg1);
	
}

@When("^I click on Hamburger menu$")
public void i_click_on_Hamburger_menu() throws Throwable {
	driver.findElement(By.className(object.getProperty("hamburger"))).click();
}

@When("^I select \"([^\"]*)\" destination from the menu list$")
public void I_select_destination_as(String arg1) throws Throwable {
	driver.findElement(By.xpath("//*[@class='di-link term-link'][contains(text(), '" + arg1 + "')]")).click();
}
}
